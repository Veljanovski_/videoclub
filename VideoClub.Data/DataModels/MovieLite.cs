﻿namespace VideoClub.Data.DataModels
{
    public class MovieLite
    {
        public int MovieId { get; set; }
        public string Caption { get; set; }
    }
}
